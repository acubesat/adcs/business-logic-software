import os
from os.path import join

from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
from conan.tools.files import copy
from conan.tools.scm import Git

class EmbeddedSoftwareRecipe(ConanFile):
    name = "adcs-business-logic"
    version = "1.1"
    revision_mode = "scm"

    license = "MIT"
    author = "SpaceDot - AcubeSAT, acubesat.obc@spacedot.gr"
    url = "https://gitlab.com/acubesat/adcs/business-logic-software.git"
    description = "Business Logic Software for ADCS"
    topics = ("satellite", "acubesat", "adcs", "adcs-software")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False], "logic_config_file_path": ["ANY"]}
    default_options = {"shared": False, "fPIC": True, "logic_config_file_path": ["ANY"]}

    exports_sources = "CMakeLists.txt", "src/*", "inc/*", "lib/*"
    generators = "CMakeDeps"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def layout(self):
        cmake_layout(self, build_folder=".")
        self.cpp.source.includedirs = ["inc"]

    def generate(self):
        tc = CMakeToolchain(self)
        if self.settings.arch != 'armv7':
            tc.variables["X86_BUILD"] = True
        tc.variables["BUSINESSLOGIC_CONFIGURATION"] = self.options.logic_config_file_path
        tc.presets_prefix = self.build_path.parent.name
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(self, pattern="*.hpp", src=join(self.source_folder, "inc"), dst=join(self.package_folder, "inc"), keep_path=True)
        copy(self, pattern="*.tpp", src=join(self.source_folder, "inc"), dst=join(self.package_folder, "inc"), keep_path=True)
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["adcs"]
        self.cpp_info.set_property("cmake_target_name", "adcs")
        self.cpp_info.libdirs = ["lib"]
        self.cpp_info.includedirs = ["inc"]

    def requirements(self):
        self.requires("etl/20.37.2", transitive_headers=True)
        self.requires("eigen/3.4.0", transitive_headers=True)
        if self.settings.arch != 'armv7':
            self.requires("catch2/3.6.0", transitive_headers=True)
#pragma once

#include "Eigen/Geometry"
#include <algorithm>

#include "Definitions.hpp"
// NOLINTBEGIN(readability-identifier-length)

const static float EMR = 6371.01e3;

namespace albedo {
/**
 * Transforms radians to TOMS reflectivity matrix indices
 * @param theta reflectivity data point's latitude
 * @param phi reflectivity data point's longitude
 * @return vector containing TOMS reflectivity matrix indices
    **/
    struct SphericalCoordinates {
        float theta;
        float phi;
    };
    struct GridIndices {
        int16_t rowIndex;
        int16_t columnIndex;
    };


    inline Eigen::Vector<int16_t, 2> radiansToIndices(const SphericalCoordinates &coordinates) {
        const float dx = 2 * PI / ReflectivityDataColumns;
        const float dy = PI / ReflectivityDataRows;

        auto rowIndex = static_cast<int16_t>(round((PI - dy / 2 - coordinates.phi) / dy));
        auto columnIndex = static_cast<int16_t>(round((coordinates.theta + PI - dx / 2) / dx));

        rowIndex = std::max(static_cast<int16_t>(0), rowIndex);
        columnIndex = std::max(static_cast<int16_t>(0), columnIndex);

        return {rowIndex, columnIndex};
    }

/**
 * Transforms TOMS reflectivity matrix indices to radians
 * @param i TOMS reflectivity matrix index
 * @param j TOMS reflectivity matrix index
 * @return vector containing reflectivity data point's latitude and longitude
 */
    inline Eigen::Vector2f indicesToRadians(const GridIndices& indices) {
        const float dx = 2 * PI / ReflectivityDataColumns;
        const float dy = PI / ReflectivityDataRows;

        const auto phi = static_cast<float>(PI - dy / 2 - static_cast<float>(indices.rowIndex) * dy);
        const auto theta = static_cast<float>(static_cast<float>(indices.columnIndex) * dx - PI + dx / 2.);

        return {theta, phi};
    }

/**
 * Calculates the area of a cell with indices i, j
 * @param i index i
 * @param j index j
 * @return area of a cell with indices i, j
 */
    float calculateCellArea(const GridIndices& indices);

/**
 * Calculates the angle between two grid index pairs
 * @param loopI reflectivity grid index i
 * @param loopJ reflectivity grid index j
 * @param sunIndexI sun grid index i
 * @param sunIndexJ sun grid index j
 * @return angle between two grid index pairs
 */
    float gridAngle(const GridIndices& loopIndices,const GridIndices& sunIndices);
} // namespace albedo

/**
 * Calculates the sunlight reflected off the Earth's surface
 * @param satellite satellite position in ECEF frame
 * @param sunPosition sun position in ECEF frame
 * @param reflectivityData Earth surface reflectivity data from TOMS project
 * @return sunlight reflected off the Earth's surface
 */
EarthCellsMatrix
calculateAlbedo(const Eigen::Vector3f &satellite, const Eigen::Vector3f &sunPosition,
                const EarthCellsMatrix &reflectivityData);
// NOLINTEND(readability-identifier-length)
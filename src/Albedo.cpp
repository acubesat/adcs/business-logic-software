#include "Eigen/Geometry"
#include "Eigen/Dense"
#include "MathFunctions.hpp"
#include "Albedo.hpp"

using namespace Eigen;

float albedo::calculateCellArea(const GridIndices& indices) {
    Vector2f radians = albedo::indicesToRadians(indices);

    const auto deltaPhi =static_cast<float>(deg2rad(static_cast<float>(180) / static_cast<float>(ReflectivityDataRows)));
    const auto deltaTheta = static_cast<float>(deg2rad(static_cast<float>(360) / static_cast<float>(ReflectivityDataColumns)));

    const float phiMax = radians(1) + deltaPhi / 2;
    const float phiMin = radians(1) - deltaPhi / 2;

    const float area = EMR * EMR * deltaTheta * (std::cos(phiMin) - std::cos(phiMax));
    return area;
}

float albedo::gridAngle(const albedo::GridIndices &loopIndices, const albedo::GridIndices &sunIndices) {
    const Vector2f loopRadians = albedo::indicesToRadians(loopIndices);
    const Vector2f sunRadians = albedo::indicesToRadians(sunIndices);

    const float angle = std::acos(std::sin(loopRadians(1)) * std::sin(sunRadians(1)) * std::cos(loopRadians(0) - sunRadians(0)) +
                       std::cos(loopRadians(1)) * std::cos(sunRadians(1)));

    return angle;
}

EarthCellsMatrix
calculateAlbedo(const Vector3f &satellite, const Vector3f &sunPosition,
                const EarthCellsMatrix &reflectivityData) {
    const float solarIrradiance = 1;

    Vector3f sunPositionSpherical = cartesianToSpherical(sunPosition);
    sunPositionSpherical(1) =static_cast<float>(PI) / 2 - sunPositionSpherical(1);

    albedo::SphericalCoordinates sunCoordinates = {sunPositionSpherical(0), sunPositionSpherical(1)};
    const Vector<int16_t, 2> sunIndices = albedo::radiansToIndices(sunCoordinates);

    albedo::GridIndices sunIndicesGrid = {sunIndices(0), sunIndices(1)};

    Vector3f grid;
    EarthCellsMatrix albedo;

    for (int16_t i = 0; i < ReflectivityDataRows; i++) {
        for (int16_t j = 0; j < ReflectivityDataColumns; j++) {
            albedo::GridIndices loopIndices = {i, j};

            float angleOfIncidentSolarIrradiance = albedo::gridAngle(loopIndices, sunIndicesGrid);

            angleOfIncidentSolarIrradiance = std::min(angleOfIncidentSolarIrradiance, static_cast<float>(PI / 2));

            const float incidentPower =
                    solarIrradiance * albedo::calculateCellArea(loopIndices) * std::cos(angleOfIncidentSolarIrradiance);
            const Vector2f gridRadians = albedo::indicesToRadians(loopIndices);
            const float gridTheta = gridRadians(0);
            const float gridPhi = gridRadians(1);

            grid = sphericalToCartesian(Vector3f(gridTheta,static_cast<float>(PI / 2 - gridPhi), EMR));

            const float satelliteDistance = (satellite - grid).norm();

            const float satelliteGridAngle = std::acos(
                    (((satellite - grid) / satelliteDistance).transpose()).dot(grid) / grid.norm());
            const auto pOut = static_cast<float>((incidentPower) * reflectivityData(i, j) * std::cos(satelliteGridAngle) /
                         (PI * satelliteDistance * satelliteDistance));
            albedo(i, j) = pOut;
        }
    }

    return albedo;
}

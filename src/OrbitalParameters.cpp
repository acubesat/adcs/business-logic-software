#include "OrbitalParameters.hpp"
#include "MathFunctions.hpp"
#include "Eigen/Core"
#include <array>
#include "etl/array.h"
// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)
using namespace Eigen;

OrbitalParameters::OrbitalParameters() :
        julianDate{0},
        timeSince{0},
        satelliteECI{{0, 0, 0}},
        greenwichSiderealTime{0},
        timeGregorian{0},
        satelliteLLH{{0, 0, 0}},
        satrec{} {}

void
OrbitalParameters::calculateTime(const TLE &tle, const char typerun, const char typeinput, const char opsmode, const gravconsttype whichconst) {
    uint16_t EYear=0;
    int timeDay=0;
    int month=0;
    int day=0;
    int hour=0;
    int minute=0;

    double sec=NAN;
    double stopMfe=NAN;
    double deltaMin=NAN;

    etl::array<char, TLELineSize> tle1={};
    etl::array<char, TLELineSize> tle2={};


    strncpy(tle1.data(), tle.first.c_str(), tle1.size() - 1);
    tle1[tle1.size() - 1] = '\0';

    strncpy(tle2.data(), tle.second.c_str(), tle2.size() - 1);
    tle2[tle2.size() - 1] = '\0';

    SGP4Funcs::twoline2rv(tle1.data(), tle2.data(),
                          typerun, typeinput, opsmode, whichconst,
                          timeSince, stopMfe, deltaMin, satrec);

    julianDate = satrec.jdsatepoch + satrec.t / 1440;


    EYear = satrec.epochyr + 2000;
    timeDay = static_cast<int>(satrec.epochdays + timeSince / 1440);
    SGP4Funcs::days2mdhms_SGP4(EYear, timeDay, month, day, hour, minute, sec);
    timeGregorian = static_cast<double>(date2decimal(EYear, month, day, hour, minute, static_cast<int>(sec)));
}

void OrbitalParameters::calculateNextPosition() {

    etl::array<double, 3> nextSatelliteECI={};
    etl::array<double, 3> nextVelocity={};

    SGP4Funcs::sgp4(satrec, timeSince, nextSatelliteECI.data(), nextVelocity.data());

    // The purpose of the next operation is to "typecast" c++ arrays to Eigen vector
    for (uint8_t i = 0; i < 3; i++) {
        this->satelliteECI(i) = static_cast<float>(nextSatelliteECI[i]);
    }

    julianDate = satrec.jdsatepoch + satrec.t / 1440;
    greenwichSiderealTime = SGP4Funcs::gstime_SGP4(julianDate);

    const Vector3f satelliteECEF = eci2ecef(this->satelliteECI, greenwichSiderealTime);
    satelliteLLH = ecef2llh(satelliteECEF * 1000);
    timeSince = timeSince + 0.1 / 60;
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)